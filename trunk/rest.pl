#!/usr/local/bin/perl -U 
require Exporter;
use strict;
use lib "conf/";
use Getopt::Long;
use rackspace;
use DBI;
use REST::Client;
use JSON;
use JSON::Path;
use Perl6::Say;
use Data::Dumper;

my $nlimit = 10;
if($ARGV[0]){
        $nlimit = $ARGV[0];
}

my $sql = "SELECT * FROM video_missing  WHERE success = 2 ORDER BY mc_id LIMIT $nlimit";
my @error_debug;
my $r_file_name = 'com\/(.*)';
eval{
	my $dbh = DBI->connect("DBI:Pg:dbname=$rackspace::DB_NAME$rackspace::DB_HOST", 'pgsql', 'lmZrW1KmJB') or die "Error cannot connect to database read: $@";
	my $dbh_insert = DBI->connect("DBI:Pg:dbname=$rackspace::DB_NAME$rackspace::DB_HOST_WRITE", 'pgsql', 'lmZrW1KmJB') or die "Error cannot connect to database read: $@"; 
	if ($dbh && $dbh_insert) {
		my $rack_login = rackspace::login();
		my $sth = $dbh->prepare($sql);
		$sth->execute;
		while (my @row = $sth->fetchrow_array() ) {
			#find container (mp4, mov, wmv, flv, ebm and 3gp)
			my $url = $row[4];
			my $rackspace_archived = $row[5];
			if($url =~ /$r_file_name/io && $1){
				my $file_name = $1;
				my $str_container = substr $url,-3;
				#This function should loop through possible endpoints using the endpoint 
				while ( (my $key, my $value) = each %ENDPOINT){
					my $check = rackspace::check_file($file_name,$str_container,%$value);
					if($check){
						say "$key: $file_name\t$rackspace_archived\t$check";
						#update table
						my $sql_insert = "UPDATE video_missing SET new_url='".$check."', success= 1 WHERE mc_id=".$row[0]." AND ff_id=".$row[1]." AND fs_id=".$row[2];
						my $sth_insert = $dbh_insert->prepare($sql_insert);
						$sth_insert->execute;
						last;
					}else{
						say "$key: $file_name\t$rackspace_archived\t$check";
						#update table
						my $sql_insert = "UPDATE video_missing SET success= 0 WHERE mc_id=".$row[0]." AND ff_id=".$row[1]." AND fs_id=".$row[2];
						my $sth_insert = $dbh_insert->prepare($sql_insert);
						$sth_insert->execute;
					}
				}
			}
		}
	}
};

if ( $@ ){
        #Push error array
       push @error_debug, $@;
}

say Dumper @error_debug;

exit(0);