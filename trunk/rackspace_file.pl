#!/usr/local/bin/perl -U
#	This script will upload any new files into Rackspace cloud files
#	account.
#	Logic: Find any files from specific drop box directory
#	Check if files have been uploaded to Rackspace, if not upload them

use strict;
use URI;
use Getopt::Long;
use Net::SMTP::SSL;
use Data::Dumper;
use File::Find;
use LWP;
use Digest::MD5 qw(md5 md5_hex md5_base64);
use bytes;
use Time::HiRes;
use MongoDB;
use MongoDB::OID;
    
my $operation;
my $container;
my $f_sema = 'rackspace.lock';
my $ua;
my ($req,$res);
my $authtoken;
my $storageurl;
my $obj_name;
my $filename;
my $buffer;
my $content_length;
my $sem_out;
my @error_debug;
my ($smtp,$email_subject,$txt_mail,$email_body);
my ($today,$year,$month,$day,$hour,$min);
my $sec_end;
my $sec_start = Time::HiRes::time;
my $r_file_type = '(\.3gp)|(\.wmv)|(\.flv)|(\.mp4)|(\.webm)';
#my $r_file_type = '(\.flv)';


#Date calculation
$month = ($month<10?"0".$month:$month);
$day = ($day<10?"0".$day:$day);
$hour = ($hour<10?"0".$hour:$hour);
$min = ($min<10?"0".$min:$min);
$today = "$year-$month-$day:$hour";
	     
sub print_usage {
        print "Error in script, you need: $0 --operation <PUT GET DELETE> --container <container name from rackspace>\n";
}
sub main {
	GetOptions(
                "operation=s" => \$operation,
                "container=s" => \$container
        );
	eval{
		if(!$operation || !($operation=='PUT' && $container)){
			print_usage();
		}else{
			
			#semaphore file
			die "Error: semaphore file still opened!" if(-f $f_sema);
			print $f_sema . "\n";
			open( SEMA, "> $f_sema" ) or die "Can't open $f_sema";
			
			flock(SEMA,2) or die "Can't lock $f_sema";
			#CLOSING RESOURCES for Semaphore
			flock(SEMA,8);
			close SEMA or die "Could not close semaphore file";
			if($?){
				push @error_debug,"Error closing semaphore:".Dumper($?);
			}
	
			$ua = LWP::UserAgent->new;
			$ua->agent("PreviewNetworks/0.1 ");
			  
			    
			# Create a request
			$req = HTTP::Request->new('GET','https://auth.api.rackspacecloud.com/v1.0',
			        [   'X-Auth-User' => 'preview88',
			            'X-Auth-Key'  => 'b7824f7d1fca3801a515ee70d4bef5e4',
			        ]
			    );
			        
			 # Pass request to the user agent and get a response back
			$res = $ua->request($req);
			                
			 # Check the outcome of the response
			if ($res->is_success) {
			        print "Authorization accepted\n";
			        $authtoken = $res->headers->{'x-auth-token'};
			        $storageurl = $res->headers->{'x-storage-url'}."/video-1/";
			        
			        binmode(FILEHANDLE);
			        local(*FILEHANDLE, $/);
			        opendir DIR, "/usr/local/etc/dropbox/sup/media/whitelabel" || die "Cannot open dir 1 $!";
					
					my $connection = MongoDB::Connection->new(host => '192.168.0.5', port => 27017);
				    my $database   = $connection->cms;
				    my $collection = $database->assets;
				    
					while ( $filename = readdir(DIR) ) {
						if($filename=~/$r_file_type/o){
							#check if file exists
					        $req =  HTTP::Request->new('GET',$storageurl.$filename,
					                [       'X-Auth-Token' => $authtoken,
					                ]
					        );
					        $res = $ua->request($req);
					        if($res->code=='404'){
					        	open (FILEHANDLE, "/usr/local/etc/dropbox/sup/media/whitelabel/".$filename) || die "Error: can't open file: $!";
						        if($?){
									push @error_debug,"Error opening $filename: ".Dumper($?);
								}
						        $buffer = <FILEHANDLE>;
						        close (FILEHANDLE) || die "Error: can't close file: $!";
						
						        $content_length = length($buffer);
						        
						        #get size from mongodb
						        my $asset = $collection->find({'files.filename'=>$filename});
						        my $db_size = 0;
								$asset->fields({'files.size'=>1,'files.filename'=>1});
								
								my $obj = $asset->next;
								
								foreach my $item($obj){
							        while ( (my $key, my $value) = each %{$item}){
							        	if(ref($value) eq 'ARRAY'){
							        		foreach my $arr (@$value) {
							        			#print "File info from mongodb:".$arr->{filename} ." ".$arr->{size};
							        			if($arr->{filename} eq $filename){
							        				$db_size = $arr->{size};
							        				last;
							        			}
							        		}
							        	}
							        }
							    }
							    #if $db_size not defined delete file from server
							    if(!$db_size){
							    	#unlink("/usr/local/etc/dropbox/sup/media/whitelabel/".$filename) ;
							    	#if($!){
							    		#push @error_debug,$!;
							    		#die "$!";
							    	#}
							    }
							    if($db_size  == $content_length){
							    	#Proceed with operation
							        $req =  HTTP::Request->new('PUT',$storageurl.$filename,
							                [       'X-Auth-Token' => $authtoken,
							                        #'ETag' => $md5,
							                        'Content-Length' => $content_length,
							                ],$buffer
							        );
							        $res = $ua->request($req);
							        #delete if everything is ok
					        		if($res->code=='201'){
					        			unlink("/usr/local/etc/dropbox/sup/media/whitelabel/".$filename) ;
								    	if($!){
								    		push @error_debug,$!;
								    		die "$!";
								    	}
								    	push @error_debug,$filename." has been uploaded\n";
								    	print $filename." has been uploaded\n";
					        		}
							    }else{
							    	print "File $filename does not have matching size: DB: $db_size Server: $content_length\n";
							    }
								
					        }else{
					        	print $filename ." is already uploaded\n";
					        	unlink("/usr/local/etc/dropbox/sup/media/whitelabel/".$filename) ;
						    	if($!){
						    		push @error_debug,$!;
						    		die "$!";
						    	}
					        }
						}
					}
					closedir DIR || die "Cannot close dir 1 $!";	
			}else {
				push @error_debug, "Authorization error: ".$res->status_line."\n";
				print "Failed: ".$res->status_line, "\n";
			}
		}
	
	#checking error and sending result email
	$txt_mail = "";
	if(@error_debug){
		foreach my $error (@error_debug) {
			$txt_mail .= "$error\n";
		}
	}else{
		$txt_mail .= "Nothing to be done\n";
	}
	$email_body = "";
	
	if($txt_mail && $txt_mail !~ /Nothing to be done/o){
		$email_subject="Uploading file to Rackspace for $today";
		
		$sec_end = (Time::HiRes::time - $sec_start);
	    $email_body="Rackspace files upload:\n$txt_mail"."It took $sec_end seconds to run the script\n";
	    
	    if (not $smtp = Net::SMTP::SSL->new('smtp.gmail.com',Port => 465,Debug => 1)) {
	    	die "Could not connect to server\n";
		}
		if($?){
			push @error_debug,"Error connecting to gmail:".Dumper($?);
		}

		$smtp->auth('pr@previewnetworks.com', 'bordeaux45')
		   || die "Authentication failed!\n";
		if($?){
			push @error_debug,"Error authenticating to gmail:".Dumper($?);
		}
		$smtp->mail('pr@previewnetworks.com' . "\n");
		$smtp->to('pr@previewnetworks.com' . "\n");
		
		$smtp->data();
		$smtp->datasend("From: " . 'pr@previewnetworks.com' . "\n");
		$smtp->datasend("To: " . 'pr@previewnetworks.com' . "\n");
		$smtp->datasend("Subject: " . $email_subject . "\n");
		$smtp->datasend("\n");
		$smtp->datasend($email_body . "\n");
		$smtp->dataend();
		$smtp->quit;
		print "Email sent: $email_body";
	}
};
	#Deleting Semaphore file if no error
	#error debugging checking
	if ( $@ ){
		#Push error array
		push @error_debug, $@;
	}
	my $reg_sema = "(error)|(Error)";
	if ($email_body !~ /$reg_sema/o){
	        print "Removing semaphore file\n";
		 	$sem_out = `/bin/rm  $f_sema 2>&1`;
	}
	$sec_end = (Time::HiRes::time - $sec_start);
	print "Script done in $sec_end seconds\n";
	print "Script result:\n". Dumper @error_debug;
}

main();

exit(0);