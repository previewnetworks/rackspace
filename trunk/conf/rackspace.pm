package rackspace;
require Exporter;
use DBI;
use strict;
use warnings;
use REST::Client;
use JSON;
use JSON::Path;
use Perl6::Say;
use Data::Dumper;
use Date::Calc qw(Add_Delta_Days Delta_Days Week_of_Year Date_to_Days Day_of_Week Day_of_Week_to_Text Day_of_Week_Abbreviation);
use IO::Handle;
use File::stat;
use Digest::MD5::File qw(file_md5_hex);
 
#START DEFINING GLOBAL VARIABLES
our @ISA = qw(Exporter);
our @EXPORT = qw($TOKEN %ENDPOINT $DB_NAME $DB_HOST $DB_HOST_WRITE $DB_PWD $DB_USER @EMAIL_ADDRESS $CLOUD_USER $CLOUD_KEY $CLOUD_USER_UK $CLOUD_KEY_UK $CLOUD_LOCATION %HEADERTYPE);
our $VERSION = 1.0;

our($TOKEN, $DB_NAME, %ENDPOINT, $DB_HOST, $DB_HOST_WRITE, $DB_PWD, $DB_USER, @EMAIL_ADDRESS, $CLOUD_USER, $CLOUD_KEY,$CLOUD_LOCATION,$CLOUD_USER_UK, $CLOUD_KEY_UK, %HEADERTYPE);

$DB_NAME = "playnw";
#$DB_HOST = ";host=10.179.131.93;port=6432";
$DB_HOST = ";host=162.13.40.62;port=6432";

#$DB_HOST_WRITE = ";host=10.178.193.245;port=5432";
$DB_HOST_WRITE = ";host=95.138.180.248;port=5432";
$DB_PWD = "lmZrW1KmJB";
$DB_USER = "pgsql";
$CLOUD_USER = "preview88";
$CLOUD_KEY = "b7824f7d1fca3801a515ee70d4bef5e4";
$CLOUD_LOCATION = "usa";
$CLOUD_USER_UK = "previewnet";
$CLOUD_KEY_UK = "3886ef91cb649acccb7bf6019bea6681";

%HEADERTYPE = ('3gp' => 'video/3gp','flv' => 'video/x-flv','mov' => 'video/quicktime','mp4' => 'video/mp4','webm' => 'video/webm','wmv' => 'video/x-ms-wmv');

#sub to replace entities
sub encode_entities{
		my $string= shift;

		$string =~ s/&/&#38;/g;
		$string =~ s/</&#60;/g;
		$string =~ s/>/&#62;/g;

		return $string;
}

#get date diff
sub get_diff_date{
	my @date_0 = split '-',$_[0];
	my @date_1 = split '-',$_[1];
	
	#looping through date
	my @start_date = ($date_0[0], $date_0[1], $date_0[2]);
	my @end_date = ($date_1[0], $date_1[1], $date_1[2]);
	
	my $Dd = Delta_Days(@start_date, @end_date);
	return $Dd;
}
#Settings time stamp
sub get_date{
	my @months = qw(Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec);
	my @weekDays = qw(Sun Mon Tue Wed Thu Fri Sat Sun);
	our ($second, $minute, $hour, $dayOfMonth, $month, $yearOffset, $dayOfWeek, $dayOfYear, $daylightSavings) = gmtime();
	my $year = 1900 + $yearOffset;
	#need to increase month no as it starts from 0
	$month = $month + 1;
	$month = '0'.$month if $month <10;
	if($_[0]){
		return "$year-$month-$dayOfMonth";
	}else{
		return "$year-$month-$dayOfMonth"."T"."$hour:$minute:$second"."Z";
	}
	
}
sub login {
	my $base_url;
	my $headers;
	my $client = REST::Client->new();
	if ($CLOUD_LOCATION eq 'uk') {
        $base_url = 'https://lon.identity.api.rackspacecloud.com';
        $headers ='{
				   "auth":
				   {
				      "RAX-KSKEY:apiKeyCredentials":
				      {
				         "username": "'.$CLOUD_USER_UK.'",
				         "apiKey": "'.$CLOUD_KEY_UK.'"
				      }
				   }
				}';
    }else {
        $base_url = 'https://identity.api.rackspacecloud.com';
        $headers ='{
				   "auth":
				   {
				      "RAX-KSKEY:apiKeyCredentials":
				      {
				         "username": "'.$CLOUD_USER.'",
				         "apiKey": "'.$CLOUD_KEY.'"
				      }
				   }
				}';
    }
	$client->setHost($base_url);
	$client->POST("/v2.0/tokens", $headers, { "Accept" => 'application/json',"Content-type" => 'application/json'});
	
	my $json = JSON->new->allow_nonref;
	$json->incr_parse();
	my $response = $json->decode( $client->responseContent() );
	#say Dumper $response;
	if($response){
		$TOKEN = $response->{'access'}->{'token'}->{'id'};
		#get endpoints for this account
		my $arr_service = $response->{'access'}->{'serviceCatalog'};
		my @service = @$arr_service;
		my $arr_endpoint;
		my @endpoint;
		for my $a (0 .. $#service){
			if($service[$a]->{'name'} eq 'cloudFilesCDN'){
				#get public url
				$arr_endpoint = $service[$a]->{'endpoints'};
				@endpoint = @$arr_endpoint;
				for my $b (0 .. $#endpoint){
					$ENDPOINT{$endpoint[$b]->{'region'}}{'cloudFilesCDN'} = $endpoint[$b]->{'publicURL'};
				}
			}elsif($service[$a]->{'name'} eq 'cloudFiles'){
				$arr_endpoint = $service[$a]->{'endpoints'};
				@endpoint = @$arr_endpoint;
				for my $b (0 .. $#endpoint){
					$ENDPOINT{$endpoint[$b]->{'region'}}{'cloudFiles'} = $endpoint[$b]->{'publicURL'};
				}	
			}
		}
		undef @service;
		undef $arr_service;
		undef @endpoint;
		undef $arr_endpoint;
	}
    return $response;
}
#Check if file exist on Rackspace endpoint, return small integer 0 or 1
#Need authentification for token and list of available endpoints
sub send_file{
	if($TOKEN && %ENDPOINT){
		my ($file, $local_file,$dir,$endpoint,$endpointcdn) = @_;
		my $md5_hex = file_md5_hex($local_file);
		my $stat    = stat($local_file) or return 0;
		my $size    = $stat->size;
		my $headers ={
					   "X-Auth-Token" => $TOKEN,
					   "ETag" => $md5_hex,
					   "Content-Type" => $HEADERTYPE{$dir},
					   "Content-length" => $size
					};
		#my $curl = `/usr/local/bin/curl -o/dev/null -f -X PUT -T $local_file -H "ETag: $md5_hex" -H "Content-type: $HEADERTYPE{$dir}" -H "X-Auth-Token: $TOKEN" $endpoint/$dir/$file`;
        
		my $client = REST::Client->new();
		my $whole_file = _content_sub($local_file);
		
		#Get checkpoint
		$client->setHost($endpoint);
	  	
	  	#send file to Rackspace
		$client->PUT("/$dir/$file", $whole_file,$headers);
	    
	    my $code = $client->responseCode();
		if($code eq '201'){
			my $headers ={
					   "X-Auth-Token" => $TOKEN
					};
			$client->setHost($endpointcdn);
	    	$client->HEAD("/$dir?format=json", $headers);
	    	my $cdnuri = $client->responseHeader('X-Cdn-Uri');
	    	return $cdnuri."/".$file;
		}
		return 0;
	}
	return 0;
}

sub _content_sub {
	my $local_file = shift;
	open my $fh, '<', $local_file or die $!;
	my $whole_file = do { local $/; <$fh> };
	return $whole_file;
}

#Check if file exist on Rackspace endpoint, return small integer 0 or 1
#Need authentification for token and list of available endpoints
sub check_file{
	if($TOKEN && %ENDPOINT){
		my ($file, $dir,%endpoint) = @_;
		my $base_url;
		my $headers ={
					   "X-Auth-Token" => $TOKEN
					};
		my $client = REST::Client->new();
		
		#Checking every checkpoint
		$base_url = $endpoint{'cloudFiles'};
		$client->setHost($base_url);
	    my $pn_file = check_pn_file($file,$dir,$client,$headers);
	    
		if($pn_file){
			#get cdn url
			$base_url = $endpoint{'cloudFilesCDN'};
	    	$client->setHost($base_url);
	    	$client->HEAD("/$dir?format=json", $headers);
	    	my $cdnuri = $client->responseHeader('X-Cdn-Uri');
	    	return $cdnuri."/".$pn_file;
		}
		
		return 0;
	}
	return 0;
}

sub check_pn_file{
	my ($file, $dir, $client,$headers) = @_;
	#need to loop through possible name
    my @sfile = split '_', $file;
    my @type = ('1','2','a','b');
    foreach(@type) {
    	$file = $sfile[0]."_".$sfile[1]."_".$_."_".$sfile[3];
		$client->HEAD("/$dir/$file?format=json", $headers);
		my $contentlength = $client->responseHeader('content-length');
		my $code = $client->responseCode();
		if(($code eq '200' || $code eq '206') && $contentlength > 0){
			return $file;
		}
	}
	return 0;
}

1;
__END__;
