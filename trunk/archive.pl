#!/usr/local/bin/perl -U 
require Exporter;
use strict;
use lib "/usr/local/etc/rackspace/trunk/conf/";
use Getopt::Long;
use rackspace;
use DBI;
use REST::Client;
use JSON;
use JSON::Path;
use Perl6::Say;
use Data::Dumper;
use Mail::Mailer;

my $NLIMIT = 100;
my $INTERVAL = 7;

if($ARGV[0]){
        $NLIMIT = $ARGV[0];
}

if($ARGV[1]){
        $INTERVAL = $ARGV[1];
}
#Date calculation
my $tyear = (localtime)[5] + 1900;
my $tmonth = (localtime)[4] + 1;
my $tday = (localtime)[3];
my $thour = (localtime)[2];
my $tmin = (localtime)[1];
$tmonth = ($tmonth<10?"0".$tmonth:$tmonth);
$tday = ($tday<10?"0".$tday:$tday);
$thour = ($thour<10?"0".$thour:$thour);
$tmin = ($tmin<10?"0".$tmin:$tmin);
my $ttoday = "$tyear-$tmonth-$tday:$thour";

my $sql = "SELECT * FROM movie_clip_file WHERE (archived = '' OR archived IS NULL) AND status_id = 2 AND published_date <= (NOW() - INTERVAL '".$INTERVAL." months') AND mc_id NOT IN(SELECT mc_id FROM video_missing) ORDER by mc_id DESC LIMIT $NLIMIT";
my @error_debug;
my $r_file_name = 'com\/(.*)';
my $r_country = '\/\/(.*?)\.';
my %arr_hd=('ch'=>'media1','ch-fr'=>'media1','de'=>'media1','dk'=>'media1','es'=>'media2','fin'=>'media2','fr'=>'media2','it'=>'media2','nl'=>'media2','se'=>'media2','uk'=>'media2');
my $notexist = 0;
my $exist = 0;
#eval{
	my $dbh = DBI->connect("DBI:Pg:dbname=$rackspace::DB_NAME$rackspace::DB_HOST", 'pgsql', 'lmZrW1KmJB') or die "Error cannot connect to database read: $@";
	my $dbh_insert = DBI->connect("DBI:Pg:dbname=$rackspace::DB_NAME$rackspace::DB_HOST_WRITE", 'pgsql', 'lmZrW1KmJB') or die "Error cannot connect to database read: $@"; 
	if ($dbh && $dbh_insert) {
		#location to uk for archiving
		$rackspace::CLOUD_LOCATION = "uk";
		my $rack_login = rackspace::login();
		my $sth = $dbh->prepare($sql);
		$sth->execute;
		while (my @row = $sth->fetchrow_array() ) {
			#find container (mp4, mov, wmv, flv, ebm and 3gp)
			my $url = $row[3];
			my $rackspace_archived = $row[5];
			if($url =~ /$r_file_name/io && $1){
				my $file_name = $1;
				my $str_container = substr $url,-3;
				$str_container = "webm" if($str_container eq 'ebm');
				if($url =~ /$r_country/io && $1){
					my $country = $1;
					$country = "se" if($country eq 'sv');
					
					my $local_filename = "/usr/local/www/apache22/data/".$arr_hd{$country}."/$country/$str_container/$file_name";
					#checking file exists locally
					my $new_file_name = check_file($file_name,$country,$str_container,%arr_hd);
					if (!$new_file_name) {
 						say "File $local_filename\t$new_file_name not on filesystem!";
 						push @error_debug,"File $local_filename\t$new_file_name not on filesystem!";
 						#insert record into video_missing
 						my $sql_insert = "INSERT INTO video_missing (mc_id,ff_id,fs_id,url,success) VALUES(".$row[0].",".$row[1].",".$row[2].",'".$row[3]."',2);";
						my $sth_insert = $dbh_insert->prepare($sql_insert);
						$sth_insert->execute;
						$notexist++;
 					}else{
 						#send data to Rackspace and update table with new url
 						#testing
 						my $s_file = rackspace::send_file($file_name,$new_file_name,$str_container,$rackspace::ENDPOINT{'LON'}{'cloudFiles'},$rackspace::ENDPOINT{'LON'}{'cloudFilesCDN'});
 						print "$url has been moved to $s_file\t";
 						if($s_file){
 							my $sql_update = "UPDATE movie_clip_file SET archived='".$s_file."' WHERE mc_id=".$row[0]." AND ff_id=".$row[1]." AND fs_id=".$row[2];
							my $sth_update = $dbh_insert->prepare($sql_update);
							my $rv = $sth_update->execute;
							if($rv){
								#unlink file on server
								if(!unlink $new_file_name){
									$sql_update = "UPDATE movie_clip_file SET archived='' WHERE mc_id=".$row[0]." AND ff_id=".$row[1]." AND fs_id=".$row[2];
									$sth_update = $dbh_insert->prepare($sql_update);
									$rv = $sth_update->execute;
								}
								push @error_debug,"$url has been moved to $s_file and $new_file_name has been removed from server";
								say "$new_file_name";
							}
 						}else{
 							push @error_debug,"$url File could not be uploaded to Rackspace";
 							say "Not Uploaded";
 						}
 						$exist++;
 					}
				}
			}
		}
		say "Upload error: $notexist\tUpload success: $exist";
	}
	
sub check_file{
	my ($file, $country, $str_container,%arr_hd) = @_;
	#need to loop through possible name
    my @sfile = split '_', $file;
    my @type = ('1','2','a','b');
    foreach(@type) {
    	$file = $sfile[0]."_".$sfile[1]."_".$_."_".$sfile[3];
    	my $local_filename = "/usr/local/www/apache22/data/".$arr_hd{$country}."/$country/$str_container/$file";
    	if (-e $local_filename) {
			return $local_filename;
		}
	}
	return 0;
}
#};

if ( $@ ){
        #Push error array
       push @error_debug, $@;
}
#UPLOAD file to ftp and if success update table (catch error)
my $txt_mail = "Output for Archiving log:\n";
if(@error_debug){
        foreach my $error (@error_debug) {
                $txt_mail .= "$error\n";
        }
}

if($txt_mail){
        my $email_subject="Archiving $ttoday";
        my $email_body="$txt_mail";
        print "Sending email\n";
        my $mailer = Mail::Mailer->new('smtp', Server => "log.filmtrailer.com");
    $mailer->open({ From    => 'patrick.rodies@rightster.com',
                    To      => 'patrick.rodies@rightster.com',
                    Subject => $email_subject,
                  }) or die "Can't open: $!\n";
     print Dumper($mailer);

    print $mailer $email_body;
        $mailer->close();
}

exit(0);